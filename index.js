
const FIRST_NAME = "Denisa Andreea";
const LAST_NAME = "Calota";
const GRUPA = "1075";

/**
 * Make the implementation here
 */
function initCaching() {

   var cache = {  
    a:{
        about: 0, 
        contact: 0, 
        home: 0
    }
   }

   cache.pageAccessCounter =function(pagina)
   {
       if (pagina === undefined)
            cache.a.home = cache.a.home + 1;
       else
       {
            if( pagina.toLowerCase().localeCompare("about")==0)
            {
                cache.a.about = cache.a.about +1;
            }
            if(pagina.toLowerCase().localeCompare("contact")==0)
            {   
                cache.a.contact = cache.a.contact +1;
            }  
       }
            
   };
  

   cache.getCache = function()
   {
        return cache.a;
   }
   return cache;
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    initCaching
}

